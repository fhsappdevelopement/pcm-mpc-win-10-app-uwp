﻿using PCM.MPC;
using PCM.MPC.Pages;
using PCM.MPC.WCFRest;
using PCM.MPC.WCFRest.Data;
using PCM.MPC.WCFRest.Serializer;
using PCM.REST.Client;
using System;
using System.ComponentModel;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Core;
using Windows.Networking.Connectivity;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace PCM
{

    /// <summary>
    /// Main class of this app. German comments are default comments from the MS template.
    /// </summary>
    sealed partial class App : Application, INotifyPropertyChanged
    {
        /// <summary>
        /// PCM Webservice URI.
        /// </summary>
        public const string WEBSERVICE_URI = "http://www.procaremanagement.de/MPCRestDataService/MPCRestDataService.svc";

        /// <summary>
        /// Property changed event handler.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Main class for business logic.
        /// </summary>
        public MobileProcarementCenter mobileProcarementCenter { get; private set; }

        /// <summary>
        /// ConnectionProfile property.
        /// </summary>
        public ConnectionProfile InternetConnectionProfile { get; private set; }

        /// <summary>
        /// Store for HasInternet property.
        /// </summary>
        private bool hasInternet;

        /// <summary>
        /// HasInternet property.
        /// </summary>
        public bool HasInternet
        {
            get
            {
                return this.hasInternet;
            }
            private set
            {
                this.hasInternet = value;
                notifyPropertyHasChanged("HasInternet");
            }
        }


        /// <summary>
        /// Initialisiert das Singletonanwendungsobjekt.  Dies ist die erste Zeile von erstelltem Code
        /// und daher das logische Äquivalent von main() bzw. WinMain().
        /// </summary>
        public App()
        {
            Microsoft.ApplicationInsights.WindowsAppInitializer.InitializeAsync(
                Microsoft.ApplicationInsights.WindowsCollectors.Metadata |
                Microsoft.ApplicationInsights.WindowsCollectors.Session);

            this.InitializeComponent();
            this.Suspending += OnSuspending;

            this.initConnectionUtils();
            this.initProcarement();
        }

        /// <summary>
        /// Inits the main class for business logic.
        /// </summary>
        private void initProcarement()
        {
            var basicRestService = new PCMRestServiceBuilder(WEBSERVICE_URI).build();

            this.mobileProcarementCenter = new MobileProcarementCenter(basicRestService);
        }

        /// <summary>
        /// Some utils for the detection of a valid internet access (event handler on changes, etc)
        /// </summary>
        /// Bem: it doesnt works in a windows 10 emulator because of emulated
        /// network connection throgh the hyper v manager.
        private void initConnectionUtils()
        {
            this.InternetConnectionProfile = NetworkInformation.GetInternetConnectionProfile();
            // init Internate state
            this.UpdateHasInternetState();

            // listen on Network Internet Changes
            NetworkInformation.NetworkStatusChanged += (object sender) =>
            {
                this.UpdateHasInternetState();
            };
        }

        /// <summary>
        /// Updates the HasInternet property.
        /// </summary>
        private void UpdateHasInternetState()
        {
            this.HasInternet = this._hasInternet();
        }

        /// <summary>
        /// Checks if internet access is available.
        /// </summary>
        /// <returns></returns>
        private bool _hasInternet()
        {
            var level = this.InternetConnectionProfile.GetNetworkConnectivityLevel();
            return this.InternetConnectionProfile != null &&
                (level == NetworkConnectivityLevel.InternetAccess || level == NetworkConnectivityLevel.ConstrainedInternetAccess);
        }

        /// <summary>
        /// Wird aufgerufen, wenn die Anwendung durch den Endbenutzer normal gestartet wird. Weitere Einstiegspunkte
        /// werden z. B. verwendet, wenn die Anwendung gestartet wird, um eine bestimmte Datei zu öffnen.
        /// </summary>
        /// <param name="e">Details über Startanforderung und -prozess.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;

            // App-Initialisierung nicht wiederholen, wenn das Fenster bereits Inhalte enthält.
            // Nur sicherstellen, dass das Fenster aktiv ist.
            if (rootFrame == null)
            {
                // Frame erstellen, der als Navigationskontext fungiert und zum Parameter der ersten Seite navigieren
                rootFrame = new Frame();

                rootFrame.NavigationFailed += OnNavigationFailed;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Zustand von zuvor angehaltener Anwendung laden
                }

                // Den Frame im aktuellen Fenster platzieren
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
                // Wenn der Navigationsstapel nicht wiederhergestellt wird, zur ersten Seite navigieren
                // und die neue Seite konfigurieren, indem die erforderlichen Informationen als Navigationsparameter
                // übergeben werden
                rootFrame.Navigate(typeof(MainPage), e.Arguments);
            }

            //register Hardware Backbutton Event 
            SystemNavigationManager.GetForCurrentView().BackRequested += App_BackRequested;

            // Sicherstellen, dass das aktuelle Fenster aktiv ist
            Window.Current.Activate();
        }

        /// <summary>
        /// Wird aufgerufen, wenn die Navigation auf eine bestimmte Seite fehlschlägt
        /// </summary>
        /// <param name="sender">Der Rahmen, bei dem die Navigation fehlgeschlagen ist</param>
        /// <param name="e">Details über den Navigationsfehler</param>
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        /// <summary>
        /// Wird aufgerufen, wenn die Ausführung der Anwendung angehalten wird.  Der Anwendungszustand wird gespeichert,
        /// ohne zu wissen, ob die Anwendung beendet oder fortgesetzt wird und die Speicherinhalte dabei
        /// unbeschädigt bleiben.
        /// </summary>
        /// <param name="sender">Die Quelle der Anhalteanforderung.</param>
        /// <param name="e">Details zur Anhalteanforderung.</param>
        private async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();

            Frame rootFrame = Window.Current.Content as Frame;
            await mobileProcarementCenter.Logout();
            rootFrame.Navigate(typeof(MainPage), e);

            //TODO: Anwendungszustand speichern und alle Hintergrundaktivitäten beenden
            deferral.Complete();
        }

        private void App_BackRequested(object sender, BackRequestedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;
            // prevents using backbutton in Login-screen to bypass login
            if (this.mobileProcarementCenter.UserInfo == null)
            {
                rootFrame.Navigate(typeof(MainPage), e);
            }
            else {
                if (rootFrame != null)
                {
                    if (rootFrame.CanGoBack && e.Handled == false)
                    {
                        e.Handled = true;
                        rootFrame.GoBack();
                    }
                }
            }
        }

        private async void notifyPropertyHasChanged(string propertyName)
        {
            var handler = this.PropertyChanged;

            if (handler != null)
            {
                // run the property changed handler in the UI thread
                // to prevent a thread violation error (0x8001010E (RPC_E_WRONG_THREAD))
                await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                });
            }
        }
    }
}
