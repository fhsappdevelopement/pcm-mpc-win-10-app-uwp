﻿namespace PCM.MPC
{
    /// <summary>
    /// This class is near to PCM.MPC.WCFRest.Data.InfoResponse but should be extended by 
    /// Vorname, Nachname, etc. in later versions.
    /// </summary>
    public class UserInfo
    {
        /// <summary>
        /// MPC Restservice SessionGUID property.
        /// </summary>
        public string SessionGUID { get; set; }

        /// <summary>
        /// MPC "Mandanten ID" MID Property.
        /// </summary>
        public int MID { get; set; }

        /// <summary>
        /// MpcPsID Property. 
        /// </summary>
        /// <value>ID from the App user.</value>
        public int MpcPsID { get; set; }

        /// <summary>
        /// Username Property.
        /// </summary>
        /// <value>Username of the App user.</value>
        public string Username { get; set; }


        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="sessionGuid">Webservice Session GUID.</param>
        /// <param name="mid">Mandanten ID.</param>
        /// <param name="mpcPsID">App User PSID.</param>
        /// <param name="username">Username.</param>
        public UserInfo(string sessionGuid, int mid, int mpcPsID, string username)
        {
            this.SessionGUID = sessionGuid;
            this.MID = mid;
            this.MpcPsID = mpcPsID;
            this.Username = username;
        }
    }
}
