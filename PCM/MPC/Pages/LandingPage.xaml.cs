﻿using PCM.MPC.Pages.TalkService;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace PCM.MPC.Pages
{
    /// <summary>
    /// Controller of the LandingPage.xaml
    /// </summary>
    public sealed partial class LandingPage : Page
    {
        /// <summary>
        /// property of the app
        /// </summary>
        private App app;

        /// <summary>
        /// default constructor
        /// </summary>
        public LandingPage()
        {
            this.app = App.Current as App;
            this.InitializeComponent();
        }

        /// <summary>
        /// Triggerd when MessengerChatButton gets clicked
        /// </summary>
        /// <param name="sender">object of the clicked button</param>
        /// <param name="e">RoutedEvent arguments</param>
        private void MessengerChatButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(ContactListPage));
        }

        /// <summary>
        /// Triggerd when LogoutButton gets clicked
        /// </summary>
        /// <param name="sender">object of the clicked button</param>
        /// <param name="e">RoutedEvent arguments</param>
        private async void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            await this.app.mobileProcarementCenter.Logout();
            this.Frame.Navigate(typeof(MainPage), e);
        }
    }
}
