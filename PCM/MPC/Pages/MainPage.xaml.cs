﻿using PCM.MPC.WCFRest;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;


namespace PCM.MPC.Pages
{
    /// <summary>
    /// Controller of MainPage.xaml
    /// </summary>
    public sealed partial class MainPage : Page, INotifyPropertyChanged
    {
        /// <summary>
        /// Property of the app
        /// </summary>
        private App app;

        /// <summary>
        /// Property of PreValidationLoginData class
        /// </summary>
        private TalkService.Miscellaneous.PreValidationLoginData preValidationLoginData;

        /// <summary>
        /// Property of PropertyChangedEventHandler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;


        /// <summary>
        /// Method to check if the app has internetconntection
        /// </summary>
        public bool HasInternet
        {
            get
            {
                return this.app.HasInternet;
            }
        }

        /// <summary>
        /// Default Contstructor
        /// </summary>
        public MainPage()
        {
            this.app = App.Current as App;
            this.InitializeComponent();
            this.preValidationLoginData = new TalkService.Miscellaneous.PreValidationLoginData();
            this.showNoInternetMessageIfNeeded();
            
            // forward HasInternet property changed from the App class 
            this.app.PropertyChanged += (object sender, PropertyChangedEventArgs e) =>
            {
                if (e.PropertyName == "HasInternet" && this.PropertyChanged != null)
                {
                    // careful! here is e used, becaused the property in this class
                    // has the same name as in the app class
                    this.PropertyChanged(this, e);
                    this.showNoInternetMessageIfNeeded();
                }
            };
        }

        /// <summary>
        /// Shows Message of no internetconnection
        /// </summary>
        private void showNoInternetMessageIfNeeded()
        {
            if (!this.HasInternet)
            {
                this.setLoginMessageTextBlockText("Keine Internetverbindung vorhanden!");
            }
        }

        /// <summary>
        /// Triggerd by clickevent on Loginbutton
        /// </summary>
        /// <param name="sender">Object of the clicked button</param>
        /// <param name="e">Routedevent arguments</param>
        private async void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            var username = this.UsernameTextBox.Text;
            var password = Base64Converter.toUrlBase64(this.passwordBox.Password);
            var mandanID = this.MandantenIdTextBox.Text;

            this.hideLoginMessage();

            if (!this.isValidLoginData(username, password, mandanID))
            {
                this.setLoginMessageTextBlockText(this.preValidationLoginData.ValidationResult);
            }
            else if (!this.HasInternet)
            {
                this.showNoInternetMessageIfNeeded();
            }
            else
            {
                var mID = 0;
                Int32.TryParse(mandanID, out mID);

                await this.processLogin(this.buildCredentials(username, password, mID));
            }
        }

        /// <summary>
        /// Method of the process of the Login
        /// </summary>
        /// <param name="credentials">Credentials which used for login</param>
        /// <returns></returns>
        private async Task processLogin(MPCCredentials credentials)
        {
            this.startProgressAnimation();

            try
            {
                // throws exception if login failed
                await this.app.mobileProcarementCenter.Authenticate(credentials);
                this.Frame.Navigate(typeof(LandingPage));
            }
            catch (MPCRestServiceException authenticationException)
            {
                this.setLoginMessageTextBlockText(authenticationException.Message);
            }

            this.endProgressAnimation();
        }

        /// <summary>
        /// Method which prevalidates login input of the user
        /// </summary>
        /// <param name="username">Username input</param>
        /// <param name="encodedPassword">Encoded password input</param>
        /// <param name="mandatenID">MandantenID input</param>
        /// <returns></returns>
        private bool isValidLoginData(string username, string encodedPassword, string mandatenID)
        {
            return this.preValidationLoginData.isLoginDataValid(username, encodedPassword, mandatenID);
        }

        /// <summary>
        /// Method to set Message for LoginMessageTextBlock
        /// </summary>
        /// <param name="text">String of the Message</param>
        private void setLoginMessageTextBlockText(string text)
        {
            var strippedText = text.Trim();
            if(strippedText != null && strippedText.Length > 0)
            {
                if (this.loginMessageContainer.Visibility == Visibility.Collapsed)
                {
                    this.loginMessageContainer.Visibility = Visibility.Visible;
                }

                this.loginMessageTextBlock.Text = strippedText;
            }
        }

        /// <summary>
        /// Hides LoginMessageTextBlock Element
        /// </summary>
        private void hideLoginMessage()
        {
            if (this.loginMessageContainer.Visibility == Visibility.Visible)
            {
                this.loginMessageContainer.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Method to start the login progress animation
        /// </summary>
        private void startProgressAnimation()
        {
            this.LoginFormContainer.Visibility = Visibility.Collapsed;
            this.EtablishConnectionContainer.Visibility = Visibility.Visible;
            this.loginProgressRing.IsActive = true;
        }

        /// <summary>
        /// Method to end the login progress animation
        /// </summary>
        private void endProgressAnimation()
        {
            this.LoginFormContainer.Visibility = Visibility.Visible;
            this.EtablishConnectionContainer.Visibility = Visibility.Collapsed;
            this.loginProgressRing.IsActive = false;
        }

        /// <summary>
        /// Method to build MPCCredentials class out of userinput
        /// </summary>
        /// <param name="username">Username input</param>
        /// <param name="encodedPassword">Encoded passwort input</param>
        /// <param name="mandantenID">MandantenID input</param>
        /// <returns></returns>
        private MPCCredentials buildCredentials(string username, string encodedPassword, int mandantenID)
        {
            return new MPCCredentials(username, encodedPassword, mandantenID);
        }
    }
}
