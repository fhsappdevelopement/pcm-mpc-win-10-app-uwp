﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace PCM.MPC.Pages
{
    /// <summary>
    /// Controller of the SettingsPage.xaml
    /// </summary>
    public sealed partial class SettingsPage : Page
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public SettingsPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// triggerd when backbutton is clicked
        /// </summary>
        /// <param name="sender">object of the clicked button</param>
        /// <param name="e">RoutedEvent arguments</param>
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }

    }
}
