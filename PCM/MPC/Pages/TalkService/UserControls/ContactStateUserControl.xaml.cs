﻿using PCM.MPC.Messenger.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace PCM.MPC.Pages.TalkService.UserControls
{
    /// <summary>
    /// Controller of the view/page for the contactstate
    /// </summary>
    public sealed partial class ContactStateUserControl : UserControl
    {
        /// <summary>
        /// A list of contactstates an their colors
        /// </summary>
        public static Dictionary<ContactState.States, string> ContactStateColors { get; set; }

        /// <summary>
        /// store for CurrentContactState
        /// </summary>
        private ContactState currentContactState;

        /// <summary>
        /// Current ContactState
        /// </summary>
        public ContactState CurrentContactState
        {
            get
            {
                return this.currentContactState;
            }
            set
            {
                if (this.currentContactState != value)
                {
                    this.currentContactState = value;
                    this.onContactStatePropertyChanged();
                }
            }
        }

        /// <summary>
        /// initializes colors of the contactstates
        /// </summary>
        static ContactStateUserControl()
        {
            ContactStateColors = new Dictionary<ContactState.States, string>();

            // default values
            ContactStateColors.Add(ContactState.States.AVAILABLE, "#78ff03");
            ContactStateColors.Add(ContactState.States.OCCUPIED, "#ff7800");
            ContactStateColors.Add(ContactState.States.OFFLINE, "#ff0000");
            ContactStateColors.Add(ContactState.States.ALSO_OFFLINE, "#ff0000");
            ContactStateColors.Add(ContactState.States.HAS_APPOINTMENT, "#ffcd03");
            ContactStateColors.Add(ContactState.States.DONT_DISTURB, "#fffc00");
        }

        /// <summary>
        /// default constructor 
        /// </summary>
        public ContactStateUserControl()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Event which gets triggerd if a state of the current contact has changed
        /// </summary>
        /// <param name="sender">current contact object</param>
        /// <param name="e">PropertyChanged arguments</param>
        private void currentContactState_StatePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("State"))
            {
                this.changeBrush();
            }
        }

        /// <summary>
        /// add stateproperty eventlistener for current contact
        /// </summary>
        private void onContactStatePropertyChanged()
        {
            if (this.currentContactState != null)
            {
                this.changeBrush(); // change color
                // todo: prevent double event subscriptions
                this.currentContactState.PropertyChanged += currentContactState_StatePropertyChanged; // subscribe new listener for contact state changes
            }
        }

        /// <summary>
        /// change the brush with the new color
        /// </summary>
        private void changeBrush()
        {
            this.contactStateEllipse.Fill = this.createBrush(this.getColorToContactState());
        }

        /// <summary>
        /// returns Color of the current contact state
        /// </summary>
        /// <returns>string of the colorcode</returns>
        private string getColorToContactState()
        {
            return ContactStateColors[this.CurrentContactState.State];
        }

        /// <summary>
        /// creates the brush of the contact state
        /// </summary>
        /// <param name="colorCode">string of colorCode</param>
        /// <returns>object of the Brush</returns>
        private SolidColorBrush createBrush(string colorCode)
        {
            Color color = Color.FromArgb(
                (byte)0xFF,
                getColorPart(colorCode, 1),
                getColorPart(colorCode, 3),
                getColorPart(colorCode, 5)
            );

            return new SolidColorBrush(color);
        }

        /// <summary>
        /// converts the colorcode string in byte
        /// </summary>
        /// <param name="colorCode">colorcode as string</param>
        /// <param name="index">index for substring</param>
        /// <returns>colorcode in byte</returns>
        private Byte getColorPart(string colorCode, int index)
        {
            return Convert.ToByte(colorCode.Substring(index, 2), 16);
        }

        /// <summary>
        /// property used for propertychanged event
        /// </summary>
        public static readonly DependencyProperty ContactListProperty =
            DependencyProperty.Register("CurrentContactState", typeof(ContactState), typeof(ContactStateUserControl), new PropertyMetadata(null));
    }
}
