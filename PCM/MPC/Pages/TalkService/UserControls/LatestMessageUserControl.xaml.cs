﻿using PCM.MPC.Messenger;
using PCM.MPC.Messenger.Data;
using System.ComponentModel;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;


namespace PCM.MPC.Pages.TalkService.UserControls
{
    public sealed partial class LatestMessageUserControl : UserControl, INotifyPropertyChanged
    {
        private string latestMessage;
        public string LatestMessage
        {
            get
            {
                return latestMessage;
            }
            private set
            {
                if(this.latestMessage != value)
                {
                    this.latestMessage = value;
                    this.NotifyPropertyChanged("LatestMessage");
                }
            }
        }

        private int psid;

        public int PSID
        {
            get
            {
                return this.psid;
            }
            set
            {
                if(this.psid != value)
                {
                    this.psid = value;
                    this.updateLatestMessage();
                }
            }
        }

        private App app;

        private MPCMessenger Messenger {
            get
            {
                return this.app.mobileProcarementCenter.Messenger;
            }
        }

        private MessageHistory History
        {
            get
            {
                return this.Messenger.History;
            }
        }

        public LatestMessageUserControl()
        {
            this.InitializeComponent();
            this.app = App.Current as App;

            this.Messenger.HistoryLoadedEvent += () =>
            {
                this.updateLatestMessage();
            };
        }

        private void updateLatestMessage()
        {
            this.LatestMessage = this.setupLatestMessage(this.psid);
        }

        private string setupLatestMessage(int psid)
        {
            var contactHistory = History[psid];

            if (contactHistory.Any())
            {
                return contactHistory.Last().MessageBody;
            }

            return "";
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        public static readonly DependencyProperty LatestMessageProperty =
            DependencyProperty.Register("LatestMessage", typeof(string), typeof(LatestMessageUserControl), new PropertyMetadata(null));

        public static readonly DependencyProperty PSIDProperty =
            DependencyProperty.Register("PSID", typeof(int), typeof(LatestMessageUserControl), new PropertyMetadata(null));

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
