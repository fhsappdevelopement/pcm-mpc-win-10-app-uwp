﻿namespace PCM.MPC.Pages.TalkService.Emoji
{
    /// <summary>
    /// Class of Emoji containing the unicode String
    /// </summary>
    public class Emoji
    {
        /// <summary>
        /// Unicode String of the Emoji
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Constructor of the Class
        /// </summary>
        /// <param name="unicode">string of the unicode</param>
        public Emoji(string unicode)
        {
            this.Code = unicode;
        }
    }
}
