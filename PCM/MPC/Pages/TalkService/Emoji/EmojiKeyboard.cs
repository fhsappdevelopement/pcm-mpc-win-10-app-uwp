﻿using System.Collections.Generic;

namespace PCM.MPC.Pages.TalkService.Emoji
{
    public class EmojiKeyboard
    {
        /// <summary>
        /// List of Emojis
        /// </summary>
        public List<Emoji> Smilies { get; private set; }

        /// <summary>
        /// Constructor of the Class
        /// </summary>
        public EmojiKeyboard()
        {
            Smilies = new List<Emoji>();
            initSmilies();
        }

        /// <summary>
        /// initializes the Smilies
        /// </summary>
        protected void initSmilies()
        {
            foreach (var unicode in UniCodeEmoji.unicodes)
            {
                Smilies.Add(new Emoji(unicode));
            }
        }
    }
}
