﻿using PCM.MPC.Messenger;
using PCM.MPC.Messenger.Data;
using System;
using System.Linq;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using PCM.MPC.Pages.TalkService.Util;
using Windows.UI.Popups;
using Windows.Foundation;
using Windows.UI.Xaml.Media;
using PCM.MPC.Pages.TalkService.Emoji;

namespace PCM.MPC.Pages.TalkService
{
    /// <summary>
    /// Controller for the View ChatPage.xaml which shows the chat between user and a contact
    /// </summary>
    public sealed partial class ChatPage : Page
    {
        /// <summary>
        /// Main class of the App
        /// </summary>
        private App app;

        /// <summary>
        /// Class of the specific contact of this chatpage
        /// </summary>
        public Contact Contact { get; set; }

        /// <summary>
        /// property of MPCMessenger
        /// </summary>
        public MPCMessenger Messenger
        {
            get
            {
                return app.mobileProcarementCenter.Messenger;
            }
        }

        /// <summary>
        /// Store for FilePicker used to select a picture to send it
        /// </summary>
        private FileOpenPicker fileOpenPicker;

        /// <summary>
        /// Class of CameraCaptureUI which is used to make a picture to send it
        /// </summary>
        private Windows.Media.Capture.CameraCaptureUI cameraCaptureUI;

        /// <summary>
        /// Class of EmojiKeyboard which contains emojis to add it a message
        /// </summary>
        public EmojiKeyboard EmojikeyB { get; set; }

        /// <summary>
        /// FilePicker property
        /// </summary>
        private FileOpenPicker FileOpenPicker
        {
            get
            {
                // init on demand
                if (this.fileOpenPicker == null)
                {
                    this.fileOpenPicker = new FileOpenPicker();

                    var fileTypeFilterExtension = new[] { ".jpeg", ".jpg", ".bmp", ".png" };

                    foreach (string extension in fileTypeFilterExtension)
                    {
                        this.fileOpenPicker.FileTypeFilter.Add(extension);
                    }

                    this.fileOpenPicker.ViewMode = PickerViewMode.Thumbnail;
                }

                return this.fileOpenPicker;
            }
        }

        /// <summary>
        /// Constuctor which initializes the EmojiKeyboard, Camera, App...  
        /// </summary>
        public ChatPage()
        {
            this.EmojikeyB = new EmojiKeyboard();
            this.cameraCaptureUI = new Windows.Media.Capture.CameraCaptureUI();
            this.app = App.Current as App;
            this.InitializeComponent();

            this.Messenger.NewMessageIncomingEvent += Messenger_NewMessageEventListener;
            this.Messenger.NewMessageOutgoingEvent += Messenger_NewMessageEventListener;
        }

        /// <summary>
        /// Eventlistener for new Messages
        /// </summary>
        /// <param name="sender">Object of the contact which is sender of the message</param>
        /// <param name="message">Obkject of the new message</param>
        private void Messenger_NewMessageEventListener(object sender, Message message)
        {
            if (message.Contact.Equals(this.Contact))
            {
                this.scrollToBottom();

                // mark that the contact of the message has no new messages
                if (message is MessageIncoming && this.Frame.Content is ChatPage)
                {
                    message.Contact.HasNewMessage = false;
                }
            }
        }

        /// <summary>
        /// Triggers when belonging Page/view is opened
        /// </summary>
        /// <param name="args"></param>
        protected override void OnNavigatedTo(NavigationEventArgs args)
        {
            this.Contact = args.Parameter as Contact;
            // mark message as seen
            this.Contact.HasNewMessage = false;
            // load data
            this.contactNameTextBlock.Text = this.Contact.IsDivision() ? this.Contact.Division : this.Contact.FullName;
            this.MessagesListView.ItemsSource = this.Messenger.History[this.Contact.PSID];

            // scroll to bottom
            this.MessagesListView.Loaded += (object sender, RoutedEventArgs e) =>
            {
                this.scrollToBottom();
            };
        }

        /// <summary>
        /// Scolls to the last Message
        /// </summary>
        // todo: maybe scroll to last readed message ..
        private void scrollToBottom()
        {
            var items = this.MessagesListView.Items;

            if (items.Count > 0)
            {
                var lastItemInList = this.MessagesListView.Items.Last();

                if (lastItemInList == null)
                {
                    return;
                }

                this.MessagesListView.ScrollIntoView(lastItemInList);
            }
        }

        /// <summary>
        /// Triggerd when ContactProfileButton is clicked and navigates to ContactInfoPage
        /// </summary>
        /// <param name="sender">contains the contactInfo button</param>
        /// <param name="e">RoutedEvent Argements</param>
        private void ContactProfileButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(ContactInfoPage), this.Contact);
        }

        /// <summary>
        /// Triggert by clicking on the backbutton. Moves one page back.
        /// </summary>
        /// <param name="sender">contains the back button</param>
        /// <param name="e">RoutedEvent Argements</param>
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            // TODO: dunno if it will create a new page or restore the last page :/
            //this.Frame.Navigate(typeof(ContactListPage));
            if (this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
            }
            else
            {
                this.Frame.Navigate(typeof(ContactListPage)); // Fallback
            }
        }

        /// <summary>
        /// Buttonevent for sending a Message
        /// </summary>
        /// <param name="sender">object of the send Button</param>
        /// <param name="e">RoutedEvent Argements</param>
        private async void SendMessageButton_Click(object sender, RoutedEventArgs e)
        {
            //this.ChatMessageBox.Focus(FocusState.Pointer);
            if (!this.getMessageFromTextBox().Equals(""))
            {
                this.sendButtonClickedHelper = true;
                var outgoingMessage = new MessageOutgoing(this.Contact, this.getMessageFromTextBox());
                this.resetMessageTextBox();

                await this.Messenger.SendMessage(outgoingMessage);
            }
        }

        /// <summary>
        /// returns the text message which the TextBox contains
        /// </summary>
        /// <returns>String: Message in the TextBox</returns>
        private string getMessageFromTextBox()
        {
            return this.ChatMessageBox.Text;
        }

        /// <summary>
        /// clear the Messagebox
        /// </summary>
        private void resetMessageTextBox()
        {
            this.ChatMessageBox.Text = "";
        }

        /// <summary>
        /// Buttonevent for creating a picture with camera
        /// </summary>
        /// <param name="sender">object of the camera button</param>
        /// <param name="e">RoutedEvent Argements</param>
        private async void CameraButton_Click(object sender, RoutedEventArgs e)
        {
            StorageFile sf = await this.cameraCaptureUI.CaptureFileAsync(Windows.Media.Capture.CameraCaptureUIMode.Photo);
            useImage(sf);
        }

        /// <summary>
        /// Buttonevent for Filepicker to choose a picture file on the device
        /// </summary>
        /// <param name="sender">object of the filepicker button</param>
        /// <param name="e">RoutedEvent Argements</param>
        private async void FileChooserButton_Click(object sender, RoutedEventArgs e)
        {
            StorageFile sf = await this.FileOpenPicker.PickSingleFileAsync();
            useImage(sf);

        }

        /// <summary>
        /// method which codes the image of the camera or filepicker in base64 and sends it as message
        /// </summary>
        /// <param name="sf"></param>
        private async void useImage(StorageFile sf)
        {
            if (sf != null)
            {
                byte[] imageBytes = await ImageUtils.StorageFileToByte(sf);
                string baseString = ImageUtils.BitmapBytesToBase64(imageBytes);

                var outgoingImageMessage = new ImageMessageOutgoing(this.Contact, "", new ImageDescription(baseString, sf.FileType));

                await this.Messenger.SendMessage(outgoingImageMessage);
            }
        }

        /// <summary>
        /// creates a popupmenu if a ImageMessage is pressed.
        /// Popupmenu contains method to save image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Image_PointerPressed(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            string imagebody = "";
            var image = (Image)sender;

            //check if pointer is on an imageMessage pressed
            if (image.DataContext is ImageMessageOutgoing)
            {
                var data = image.DataContext as ImageMessageOutgoing;
                imagebody = data.ImageDescription.ImageBody;
            }
            else if (image.DataContext is ImageMessageIncoming)
            {
                var data = image.DataContext as ImageMessageIncoming;
                imagebody = data.ImageDescription.ImageBody;
            }

            if (imagebody.Length > 1)
            {
                var menu = new PopupMenu();
                menu.Commands.Add(new UICommand("Speichern", async (command) =>
                {
                    if (await ImageUtils.SaveBitmapImageToPictureLibrary(imagebody))
                    {
                        var dialog = new MessageDialog("Bild erfolgreich in der Bildergallery gespeichert.");
                        dialog.Title = "Speichern erfolgreich.";
                        await dialog.ShowAsync();
                    }
                }));
                await menu.ShowForSelectionAsync(GetElementRect((FrameworkElement)sender));
            }
        }

        /// <summary>
        /// method for getting the selected Element of the popupmenu
        /// </summary>
        /// <param name="element">FrameworkElement</param>
        /// <returns>selected Element of the popupmenu</returns>
        public static Rect GetElementRect(FrameworkElement element)
        {
            GeneralTransform buttonTransform = element.TransformToVisual(null);
            Point point = buttonTransform.TransformPoint(new Point());
            return new Rect(point, new Size(element.ActualWidth, element.ActualHeight));
        }

        /// <summary>
        /// Triggerd when emojibutton clicked. Makes EmojiKeyboardGrid visible or collapes it.
        /// Also set focus to an other object then the textbox so the keyboard on mobile devices fades.
        /// </summary>
        /// <param name="sender">Object of the Button</param>
        /// <param name="e">RoutedEvent argemnets</param>
        private void EmojiButton_Click(object sender, RoutedEventArgs e)
        {
            if (EmojiKeyboardGrid.Visibility == Visibility.Visible)
            {
                EmojiKeyboardGrid.Visibility = Visibility.Collapsed;
                //ChatMessageBox.Focus(FocusState.Programmatic);
            }
            else
            {
                EmojiKeyboardGrid.Visibility = Visibility.Visible;
                //Little Hack to unfocus the ChatMessageBox to the Keyboard on Mobile Devices collapse
                //MessagesListView.Focus(FocusState.Pointer);
                //Windows.UI.ViewManagement.InputPane.GetForCurrentView().TryHide();
            }
        }

        /// <summary>
        /// Triggerd when an emoji gets klicked in the emoji keyboard. adds the emoji to the textbox.
        /// </summary>
        /// <param name="sender">object of the emoji</param>
        /// <param name="e">ItemClickEvent argements</param>
        private void EmojiKeyboard_ItemClick(object sender, ItemClickEventArgs e)
        {
            var emoji = e.ClickedItem as Emoji.Emoji;
            if (emoji != null)
            {
                ChatMessageBox.Text += emoji.Code;
            }
        }

        /// <summary>
        /// Triggered when the chatmessagebox is lost the focus.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Args</param>
        private void ChatMessageBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (this.sendButtonClickedHelper)
            {
                ChatMessageBox.Focus(FocusState.Keyboard);
                this.sendButtonClickedHelper = false;
            }
        }

        private bool sendButtonClickedHelper = false;
    }
}
