﻿using PCM.MPC.Messenger.Data;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace PCM.MPC.Pages.TalkService
{
    /// <summary>
    /// Controller for ContactInfo.
    /// </summary>
    public sealed partial class ContactInfoPage : Page
    {
        /// <summary>
        /// Current Contact.
        /// </summary>
        public Contact Contact { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public ContactInfoPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// triggerd when navigated to the page and sets the contact
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.Contact = e.Parameter as Contact;
        }

        /// <summary>
        /// triggerd when backbutton is clicked
        /// </summary>
        /// <param name="sender">object of the backbutton</param>
        /// <param name="e">RoutedEvent arguments</param>
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }
    }
}
