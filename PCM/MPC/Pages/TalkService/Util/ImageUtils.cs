﻿using System;
using System.IO;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;

namespace PCM.MPC.Pages.TalkService.Util
{
    /// <summary>
    /// Class which contains methods for image usage
    /// </summary>
    static class ImageUtils
    {
        /// <summary>
        /// converts Storeagefile to BitmapImage
        /// </summary>
        /// <param name="sf">object of StorageFile</param>
        /// <returns>Task of a BitmapImage out of the StorageFile</returns>
        internal async static Task<BitmapImage> StorageFileToBitmapImage(StorageFile sf)
        {
            BitmapImage bmp = new BitmapImage();
            IRandomAccessStream stream = await sf.OpenAsync(FileAccessMode.Read);
            bmp.SetSource(stream);

            return bmp;
        }
        /// <summary>
        /// converts stiragefile to byte array
        /// </summary>
        /// <param name="savedStorageFile">object of StorageFile</param>
        /// <returns>Task of Byte Array of the StoreageFile</returns>
        internal static async Task<byte[]> StorageFileToByte(StorageFile savedStorageFile)
        {
            using (IRandomAccessStream fileStream = await savedStorageFile.OpenAsync(Windows.Storage.FileAccessMode.Read))
            {
                var dr = new DataReader(fileStream.GetInputStreamAt(0));
                var bytes = new byte[fileStream.Size];
                await dr.LoadAsync((uint)fileStream.Size);
                dr.ReadBytes(bytes);
                return bytes;
            }
        }
        /// <summary>
        /// converts byte array to bitmapimage
        /// </summary>
        /// <param name="bytes">byte array to create the image</param>
        /// <returns>bitmapimage of the byte array</returns>
        internal static async Task<BitmapImage> ByteToBitmapImage(byte[] bytes)
        {
            InMemoryRandomAccessStream ras = new InMemoryRandomAccessStream();
            Stream stream = new MemoryStream(bytes);
            BitmapImage image = new BitmapImage();

            await stream.CopyToAsync(ras.AsStreamForWrite());


            image.SetSource(ras);

            return image;
        }

        /// <summary>
        /// converts array of bytes to base64
        /// </summary>
        /// <param name="bytes">byte array</param>
        /// <returns>base64 string</returns>
        internal static string BitmapBytesToBase64(byte[] bytes)
        {
            return Convert.ToBase64String(bytes);
        }
        /// <summary>
        /// converts base64 string to bitmapimage
        /// </summary>
        /// <param name="baseString">base64 string</param>
        /// <returns>bitmapimage of the base64 string</returns>
        internal static BitmapImage Base64ToBitmap(string baseString)
        {
            using (InMemoryRandomAccessStream ms = new InMemoryRandomAccessStream())
            {
                // Writes the image byte array in an InMemoryRandomAccessStream
                // that is needed to set the source of BitmapImage.
                using (DataWriter writer = new DataWriter(ms.GetOutputStreamAt(0)))
                {
                    writer.WriteBytes(Convert.FromBase64String(baseString));

                    // The GetResults here forces to wait until the operation completes
                    // (i.e., it is executed synchronously), so this call can block the UI.
                    writer.StoreAsync().GetResults();
                }

                var image = new BitmapImage();
                image.SetSource(ms);
                return image;
            }
        }
        /// <summary>
        /// saves base64 string of an image in the picturelibrary
        /// </summary>
        /// <param name="writeImageBody">base64 string of image</param>
        /// <returns>Task of the bool result</returns>
        internal static async Task<bool> SaveBitmapImageToPictureLibrary(string writeImageBody)
        {
            byte[] byteImg = Convert.FromBase64String(writeImageBody);
            StorageFolder pictureLib = KnownFolders.PicturesLibrary;
            StorageFile emptyFileImg = await pictureLib.CreateFileAsync("pcm_" + DateTime.Now.Ticks + ".jpg", CreationCollisionOption.ReplaceExisting);

            var stream = await emptyFileImg.OpenAsync(FileAccessMode.ReadWrite);

            using (var outputStream = stream.GetOutputStreamAt(0))
            {
                DataWriter writer = new DataWriter(outputStream);
                writer.WriteBytes(byteImg);
                await writer.StoreAsync();
                await outputStream.FlushAsync();
                return true;
            }
        }
    }
}
