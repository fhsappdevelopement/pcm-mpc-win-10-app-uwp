﻿using PCM.MPC.Messenger;
using PCM.MPC.Messenger.Data;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace PCM.MPC.Pages.TalkService
{
    /// <summary>
    /// Controller of ContactListPage.xaml
    /// </summary>
    public sealed partial class ContactListPage : Page
    {
        /// <summary>
        /// property of the app
        /// </summary>
        private App app;

        /// <summary>
        /// property of MPCMessenger Class
        /// </summary>
        public MPCMessenger Messenger
        {
            get
            {
                return this.app.mobileProcarementCenter.Messenger;
            }
        }

        /// <summary>
        /// default constructor
        /// </summary>
        public ContactListPage()
        {
            this.InitializeComponent();
            this.app = App.Current as App;

            this.Messenger.NewMessageIncomingEvent += Messenger_NewMessageEvent;
            this.Messenger.NewMessageOutgoingEvent += Messenger_NewMessageEvent;
        }

        /// <summary>
        /// Event which gets triggerd by a new message
        /// </summary>
        /// <param name="sender">object of the eventsender</param>
        /// <param name="message">object of the new message</param>
        private void Messenger_NewMessageEvent(object sender, Message message)
        {
            var items = this.contactsListView.Items;
            foreach(var item in items)
            {
                if(true)
                {
                    this.contactsListView.UpdateLayout();
                }
            }
        }
        /// <summary>
        /// triggerd when navigated to the page
        /// </summary>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
           
        }

        /// <summary>
        /// Event that gets triggerd when an item is clicked in the ContactList
        /// </summary>
        /// <param name="sender">the clicked Contact object</param>
        /// <param name="e">ItemClickEvent arguments</param>
        private void ListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            var choosenContact = e.ClickedItem as Contact;

            // navigate to ChatPage
            //((Frame)Window.Current.Content).Navigate(typeof(ChatPage), choosenContact);
            this.Frame.Navigate(typeof(ChatPage), choosenContact);
        }

        /// <summary>
        /// triggerd when backbutton is clicked
        /// </summary>
        /// <param name="sender">object of the backbutton</param>
        /// <param name="e">RoutedEvent arguments</param>
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }

        /// <summary>
        /// triggerd when settingsbutton is clicked
        /// </summary>
        /// <param name="sender">object of the settingsbutton</param>
        /// <param name="e">RoutedEvent arguments</param>
        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(SettingsPage), e);
        }
    }
}
