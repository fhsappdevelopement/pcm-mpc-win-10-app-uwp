﻿using System;

namespace PCM.MPC.Pages.TalkService.Miscellaneous
{
    /// <summary>
    /// PreValidation Class for login input
    /// </summary>
    class PreValidationLoginData
    {
        /// <summary>
        /// Property of Warning String
        /// </summary>
        public const string WARNING_USERNAME_TOO_SHORT = "Der Username muss mindestens zwei Zeichen enthalten.";

        /// <summary>
        /// Property of Warning String
        /// </summary>
        public const string WARNING_PASSWORD_EMPTY = "Bitte geben sie ein Passwort ein.";

        /// <summary>
        /// Property of Warning String
        /// </summary>
        public const string WARNING_MANDANTEN_ID_NUMBER = "Die MandantenID muss eine Nummer sein.";

        /// <summary>
        /// Property of Warning String
        /// </summary>
        public const string WARNING_MANDANTEN_ID_EMPTY = "Bitte geben sie eine MandantenID ein.";

        /// <summary>
        /// Property of PreValdiationResult string
        /// </summary>
        public string PreValidationResult;

        /// <summary>
        /// Default constructor
        /// </summary>
        public PreValidationLoginData()
        {
            PreValidationResult = "";
        }

        /// <summary>
        /// Method to check if all Inputs are valid 
        /// </summary>
        /// <param name="inputUsername">Username input</param>
        /// <param name="inputPassword">Password input</param>
        /// <param name="inputMandantenID">MandantenID input</param>
        /// <returns>Bool result of Validation</returns>
        public bool isLoginDataValid(string inputUsername, string inputPassword, string inputMandantenID)
        {
            return isUsernameValid(inputUsername) && isMandantenIdValid(inputMandantenID) && isPasswordValid(inputPassword);
        }

        /// <summary>
        /// Method to validate Username input
        /// </summary>
        /// <param name="username">Username string input</param>
        /// <returns>Bool result of Validation</returns>
        private bool isUsernameValid(string username)
        {
            if (username.Length < 2)
            {
                PreValidationResult = WARNING_USERNAME_TOO_SHORT;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Method to validate Password input
        /// </summary>
        /// <param name="password">Password string input</param>
        /// <returns>Bool result of Validation</returns>
        private bool isPasswordValid(string password)
        {
            if (password == string.Empty || password.Length < 1)
            {
                PreValidationResult = WARNING_PASSWORD_EMPTY;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Method to validate MandantenID input
        /// </summary>
        /// <param name="mandantenID">MandantenID string input</param>
        /// <returns>Bool result of Validation</returns>
        private bool isMandantenIdValid(string mandantenID)
        {
            try
            {
                if (mandantenID == string.Empty || mandantenID.Length < 1)
                {
                    PreValidationResult = WARNING_MANDANTEN_ID_EMPTY;
                    return false;
                }
                int MID = Convert.ToInt32(mandantenID);
                return true;
            }
            catch
            {
                PreValidationResult = WARNING_MANDANTEN_ID_NUMBER;
                return false;
            }
        }

        /// <summary>
        /// Property of ValidationResult
        /// </summary>
        public string ValidationResult
        {
            get
            {
                return this.PreValidationResult;
            }
        }

    }
}

