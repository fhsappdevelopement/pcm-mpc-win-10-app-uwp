﻿using PCM.MPC.Messenger.Data;
using System;
using System.Linq;
using Windows.UI.Xaml.Data;

namespace PCM.MPC.Pages.TalkService.Miscellaneous
{
    /// <summary>
    /// Class to show Last Message of a contact
    /// </summary>
    class ShowLatestMessageConverter : IValueConverter
    {
        /// <summary>
        /// Property of the app
        /// </summary>
        protected App app;

        /// <summary>
        /// Property of the MessageHistory
        /// </summary>
        protected MessageHistory History
        {
            get
            {
                return app.mobileProcarementCenter.Messenger.History;
            }
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public ShowLatestMessageConverter()
        {
            this.app = App.Current as App;
        }

        /// <summary>
        /// Method to convert the string of the last message of an user
        /// </summary>
        /// <param name="value">object of the value to convert</param>
        /// <param name="targetType">type of the target to convert to</param>
        /// <param name="parameter">object of the parameter</param>
        /// <param name="language">String of the used language</param>
        /// <returns>String of the last message</returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            string result = "";
            
            if(value == null || !(value is int))
            {
                return result;
            }

            var psid = (int)value;
            var messages = this.History[psid];

            if(messages.Count <= 0) {
                return result;
            }

            var lastMessage = messages.Last();

            return this.buildLastMessagePreview(lastMessage);
        }

        /// <summary>
        /// Method to build string of the last message with max lenght 50.
        /// </summary>
        /// <param name="message">Message string</param>
        /// <returns>String of last message with max lenght</returns>
        private String buildLastMessagePreview(Message message)
        {
            var length = message.MessageBody.Length >= 50 ? 50 : message.MessageBody.Length;
            return message.MessageBody.Substring(0, length);
        }

        /// <summary>
        /// Method to Convert value back
        /// </summary>
        /// <param name="value">Object to convert back</param>
        /// <param name="targetType">type of the target to convert to</param>
        /// <param name="parameter">object of the parameter</param>
        /// <param name="language">String of the used language</param>
        /// <returns>Object of the value</returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
