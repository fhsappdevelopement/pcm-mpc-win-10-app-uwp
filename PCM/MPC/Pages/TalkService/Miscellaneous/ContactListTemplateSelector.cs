﻿using PCM.MPC.Messenger.Data;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace PCM.MPC.Pages.TalkService.Miscellaneous
{
    /// <summary>
    /// TemplateSelector for contacts and division
    /// </summary>
    class ContactListTemplateSelector : DataTemplateSelector
    {
        /// <summary>
        /// DataTemplate for Division
        /// </summary>
        public DataTemplate DivisionDataTemplate { get; set; }

        /// <summary>
        /// DataTemplate for Person
        /// </summary>
        public DataTemplate PersonDataTemplate { get; set; }

        /// <summary>
        /// Selector of the Template
        /// </summary>
        /// <param name="item">Object to choose template</param>
        /// <returns>Template of the Object</returns>
        protected override DataTemplate SelectTemplateCore(object item)
        {
            var contact = item as Contact;
            if (contact != null)
            {
                return contact.IsDivision() ? this.DivisionDataTemplate : this.PersonDataTemplate;
            }


            return null;
        }
    }
}
