﻿using System;
using Windows.UI.Text;
using Windows.UI.Xaml.Data;

namespace PCM.MPC.Pages.TalkService.Miscellaneous
{
    /// <summary>
    /// Converts boolean to FontWeihts.Normal on false and FontWeights.Bold on true.
    /// </summary>
    class BoolToFontStyleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return value is bool && (bool)value ? FontWeights.Bold : FontWeights.Normal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
