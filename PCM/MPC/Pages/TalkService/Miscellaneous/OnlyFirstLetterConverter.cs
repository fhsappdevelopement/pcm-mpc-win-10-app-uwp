﻿using System;
using Windows.UI.Xaml.Data;

namespace PCM.MPC.Pages.TalkService.Miscellaneous
{
    class OnlyFirstLetterConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var str = value as string;

            if(str != null)
            {
                return str.Substring(0, 2);
            }

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
