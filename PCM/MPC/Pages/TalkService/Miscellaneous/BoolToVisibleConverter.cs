﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace PCM.MPC.Pages.TalkService.Miscellaneous
{
    /// <summary>
    /// True to Visibility.Visible, false to Visibility.Collapsed.
    /// </summary>
    class BoolToVisibleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if(value is bool)
            {
                return (bool)value ? Visibility.Visible : Visibility.Collapsed;
            }

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
