﻿using PCM.MPC.Messenger.Data;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace PCM.MPC.Pages.TalkService.Miscellaneous
{
    /// <summary>
    /// Class to Select correct Template for Messages
    /// </summary>
    class ChatPageTemplateSelector : DataTemplateSelector
    {
        /// <summary>
        /// Property of DataTemplate for MessageIncoming
        /// </summary>
        public DataTemplate MessageIncomingTemplate { get; set; }

        /// <summary>
        /// Property of DataTemplate for MessageOutgoing
        /// </summary>
        public DataTemplate MessageOutgoingTemplate { get; set; }

        /// <summary>
        /// Property of DataTemplate for ImageMessageIncoming
        /// </summary>
        public DataTemplate ImageMessageIncomingTemplate { get; set; }

        /// <summary>
        /// Property of DataTemplate for ImageMessageOutgoing
        /// </summary>
        public DataTemplate ImageMessageOutgoingTemplate { get; set; }

        /// <summary>
        /// Method to select the Template of the message object
        /// </summary>
        /// <param name="item">message object</param>
        /// <returns>Datatemplate of the message</returns>
        protected override DataTemplate SelectTemplateCore(object item)
        {
            if (item is ImageMessageIncoming)
            {
                return this.ImageMessageIncomingTemplate;
            }

            if (item is ImageMessageOutgoing)
            {
                return this.ImageMessageOutgoingTemplate;
            }

            return item is MessageIncoming ? this.MessageIncomingTemplate : this.MessageOutgoingTemplate;
        }
    }
}
