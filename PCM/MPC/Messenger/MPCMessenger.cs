﻿using PCM.MPC.Messenger.Data;
using PCM.MPC.WCFRest;
using PCM.MPC.WCFRest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Imaging;

namespace PCM.MPC.Messenger
{
    /// <summary>
    /// The main class of the TalksService.
    /// </summary>
    /// <remarks>Inits a 5 second timer which will be frequently
    /// request the webservice to update the contactlist and to
    /// receive new messages. You can also send new messages via
    /// the SendMessage method.</remarks>
    public class MPCMessenger : IDisposable
    {
        /// <summary>
        /// RestService property.
        /// </summary>
        /// <remarks>For the communication with the webservice.</remarks>
        public IMPCInfoBaseRestService RestService { get; }

        /// <summary>
        /// History property.
        /// </summary>
        public MessageHistory History { get; }

        /// <summary>
        /// UserInfo property.
        /// </summary>
        /// <seealso cref="UserInfo"/>
        public UserInfo UserInfo { get; }

        /// <summary>
        /// Store for ContactList property.
        /// </summary>
        protected ContactList contactList;

        /// <summary>
        /// Contactlist property.
        /// </summary>
        /// <remarks>Initializes the contactlist on demand.</remarks>
        public ContactList ContactList
        {
            get
            {
                if (!this.IsContactListLoaded)
                {
                    this.UpdateContactList();
                }

                return this.contactList;
            }
        }

        /// <summary>
        /// IsContactListLoaded property.
        /// </summary>
        /// <remarks>Determines if the contactlist is loaded.</remarks>
        public bool IsContactListLoaded
        {
            get
            {
                return this.contactList.Any();
            }
        }

        /// <summary>
        /// Store for timer object.
        /// </summary>
        protected DispatcherTimer messengerTasksTimer;

        /// <summary>
        /// Constructor with 5 seconds polling interval.
        /// </summary>
        /// <param name="restService">The InfoBase Service</param>
        /// <param name="userInfo">UserInfo Data with GUID</param>
        public MPCMessenger(IMPCInfoBaseRestService restService, UserInfo userInfo) : this(restService, userInfo, 5)
        {

        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="restService">The InfoBase Service</param>
        /// <param name="userInfo">UserInfo Data with GUID</param>
        /// <param name="pollingInterval">Polling interval in seconds</param>
        public MPCMessenger(IMPCInfoBaseRestService restService, UserInfo userInfo, int pollingInterval)
        {
            this.RestService = restService;
            this.UserInfo = userInfo;

            // create an empty contactlist instance because, if it is null,
            // XAML has a null reference and not the reference to this 
            // contactlist
            this.contactList = new ContactList();
            this.History = new MessageHistory();

            this.messengerTasksTimer = new DispatcherTimer();

            // init timer
            this.initTimer(pollingInterval);
        }

        /// <summary>
        /// Initialises the 5 second timer for message retrieving.
        /// </summary>
        protected void initTimer(int pollingInterval)
        {
            // TODO: remove MagicNumber
            this.messengerTasksTimer.Interval = TimeSpan.FromSeconds(pollingInterval);
            this.messengerTasksTimer.Tick += (object sender, object e) =>
            {
                try
                {
                    this.fireCheckForNewMessagesEvent();

                    this.UpdateContactList();
                    this.RetriveMessages();

                    this.fireCheckedForNewMessagesEvent();
                }
                catch (Exception ex)
                {

                }
            };

            this.messengerTasksTimer.Start();
        }

        /// <summary>
        /// Intended to use in a timer-task or something else
        /// </summary>
        public async void UpdateContactList()
        {
            // update list to keep the reference
            // maybe use DependencyProperty
            this.contactList.Update(await this.createContactList());
        }

        /// <summary>
        /// Retrieve messages from webservice.
        /// </summary>
        /// <seealso cref="MPCRestService"/>
        public async void RetriveMessages()
        {
            var messages = await this.retrieveMessagesFromWebservice();

            foreach (MessageIncoming incomingMessage in messages)
            {
                incomingMessage.Contact.HasNewMessage = true;
                // add messages to history
                this.History.AddMessage(incomingMessage);
                this.fireNewMessageIncomingEvent(this, incomingMessage);
            }
        }

        /// <summary>
        /// Retrieve messages from webservice.
        /// </summary>
        /// <seealso cref="MPCRestService"/>
        protected async Task<List<MessageIncoming>> retrieveMessagesFromWebservice()
        {
            var talkMessages = await this.RestService.TalkMessageGetItems(this.UserInfo.MID, this.UserInfo.SessionGUID);
            var incomingMessages = new List<MessageIncoming>();

            // the MPC PSID is the ID from the App user and PCM PSID the ID 
            // from the sender as I understand.
            foreach (TalkMessage talkMessage in talkMessages.list)
            {
                var fromContact = this.contactList.GetContactByPsID(talkMessage.PCM_PSID);
                if (fromContact == null)
                {
                    // this should never happen, but it's happened. So I removed the AssertionError-like Exception
                    // and adjusted the code to make it possible to create a contact which would not be
                    // removed during the 5sec updated process (signed fruchuxs)
                    fromContact = this.buildUnknownContactWithRemoveableProtection(talkMessage.PCM_PSID);
                }

                if (talkMessage.ImageBody != null && talkMessage.ImageBody.Length > 0)
                {
                    incomingMessages.Add(new ImageMessageIncoming(fromContact, talkMessage.MessageBody, new ImageDescription(talkMessage.ImageBody, talkMessage.ImageType)));
                }
                else
                {
                    // TODO: ignore DSID and MPC_PSID - check later MPC_PSID
                    incomingMessages.Add(new MessageIncoming(fromContact, talkMessage.MessageBody));
                }
            }

            return incomingMessages;
        }

        /// <summary>
        /// Build an unknown contact.
        /// </summary>
        /// <remarks>This unknown contact is not removeable and is used for the purpose
        /// if an unknown contact tries to communicate with the app user.</remarks>
        /// <param name="pcm_psid"></param>
        /// <returns></returns>
        protected Contact buildUnknownContactWithRemoveableProtection(int pcm_psid)
        {
            return new Contact("Contact #" + pcm_psid, "Unknown", "Warning: Unknown Contact!", pcm_psid, ContactState.States.OFFLINE, null, false);
        }

        /// <summary>
        /// Sends a message immediately to the webservice.
        /// </summary>
        /// <param name="outgoingMessage">Outgoing message.</param>
        /// <returns>true if delivered.</returns>
        public async Task<bool> SendMessage(MessageOutgoing outgoingMessage)
        {
            var tmessage = new TalkMessage(this.UserInfo.MpcPsID, outgoingMessage.Contact.PSID, outgoingMessage.MessageBody);

            return await this._sendMessage(outgoingMessage, tmessage);
        }

        /// <summary>
        /// Sends a message with an image immediately to the webservice.
        /// </summary>
        /// <param name="outgoingMessage">Outgoing image message.</param>
        /// <returns>true if delivered.</returns>
        public async Task<bool> SendMessage(ImageMessageOutgoing outgoingMessage)
        {
            var tmessage = new TalkMessage(
                this.UserInfo.MpcPsID,
                outgoingMessage.Contact.PSID,
                outgoingMessage.MessageBody,
                outgoingMessage.ImageDescription.ImageBody,
                outgoingMessage.ImageDescription.ImageTyp);

            return await this._sendMessage(outgoingMessage, tmessage);
        }

        /// <summary>
        /// Sends a message with an image immediately to the webservice.
        /// </summary>
        /// <param name="outgoingMessage">Outgoing message.</param>
        /// <param name="talkMessage"><TalkMessage based on OutgoingMessage content./param>
        /// <returns></returns>
        protected async Task<bool> _sendMessage(MessageOutgoing outgoingMessage, TalkMessage talkMessage)
        {
            // add to history and rise event
            this.History.AddMessage(outgoingMessage);
            this.fireNewMessageOutgoingEvent(this, outgoingMessage);

            // start submitting and 
            outgoingMessage.StartSubmitting();

            // transfer
            ArrayOfTalkMessage arrayOfTalkMesssage = new ArrayOfTalkMessage();
            arrayOfTalkMesssage.list = new[] { talkMessage };

            try
            {
                // try transfer and set response infos to outgoing Message
                Response response = await this.RestService.TalkMessageAddItems(arrayOfTalkMesssage, this.UserInfo.MID, this.UserInfo.SessionGUID);
                outgoingMessage.Status = response.Result;
                outgoingMessage.ErrorText = response.ErrorText;

                outgoingMessage.EndSubmitting(response.Result == "ok");
            }
            catch (Exception e)
            {
                outgoingMessage.NotSubmitted(e);
            }

            return outgoingMessage.IsDelivered;
        }

        /// <summary>
        /// Create an contactlist which is filled by the webservice.
        /// </summary>
        /// <returns>Contact list.</returns>
        protected async Task<ContactList> createContactList()
        {
            ContactList contactList = new ContactList();

            string[] fileList = (await this.requestInfoImageList()).list;
            TalkContact[] talkContacts = (await this.requestArrayOfTalkContact()).list;

            foreach (TalkContact talkContact in talkContacts)
            {
                string contactImageString = this.findFileByNameInList(fileList, talkContact.PSID.ToString());
                BitmapImage contactImage = await this.requestUserInfoImage(contactImageString);

                Contact createdContact = new Contact(talkContact, contactImage);
                contactList.Add(createdContact);
            }

            return contactList;
        }

        protected string findFileByNameInList(string[] list, string search)
        {
            var arrAsList = new List<string>(list);
            var pattern = new Regex(@"^" + search + @".{3,5}$");

            try
            {
                return arrAsList.First(f => pattern.IsMatch(f));
            }
            catch (InvalidOperationException)
            {
                // oke nothing is found, return null
                return null;
            }
        }

        /// <summary>
        /// Request the ArrayOfTalkContact object.
        /// </summary>
        /// <returns>ArrayOfTalkContact.</returns>
        protected async Task<ArrayOfTalkContact> requestArrayOfTalkContact()
        {
            return await this.RestService.TalkContactGetItems();
        }

        /// <summary>
        /// Request the ArrayOfFilename object.
        /// </summary>
        /// <returns>ArrayOfFileName</returns>
        protected async Task<ArrayOfFileName> requestInfoImageList()
        {
            return await this.RestService.InfoImageGetList();
        }

        /// <summary>
        /// Request the BitmapImage object of a contact by psid.
        /// </summary>
        /// <param name="filename">filename</param>
        /// <returns>BitmapImage</returns>
        protected async Task<BitmapImage> requestUserInfoImage(string filename)
        {
            if (filename != null && filename.Length > 0)
            {
                return await this.RestService.InfoImageGetItem(filename);
            }

            return null;
        }

        /// <summary>
        /// Fires the event for a new IncomingMessage.
        /// </summary>
        /// <param name="sender">this</param>
        /// <param name="message">MessageIncoming</param>
        protected void fireNewMessageIncomingEvent(object sender, MessageIncoming message)
        {
            this.NewMessageIncomingEvent?.Invoke(sender, message);
        }

        /// <summary>
        /// Fores the event for a new OutgoingMessage.
        /// </summary>
        /// <param name="sender">this</param>
        /// <param name="message">MessageOutgoing</param>
        protected void fireNewMessageOutgoingEvent(object sender, MessageOutgoing message)
        {
            this.NewMessageOutgoingEvent?.Invoke(sender, message);
        }

        /// <summary>
        /// Fire the event that indicates that the messenger will check
        /// for new messages.
        /// </summary>
        protected void fireCheckForNewMessagesEvent()
        {
            this.CheckForNewMessagesEvent?.Invoke();
        }

        /// <summary>
        /// Fire the event which indicates, that the messenger has checked
        /// for new messages.
        /// </summary>
        protected void fireCheckedForNewMessagesEvent()
        {
            this.CheckedForNewMessagesEvent?.Invoke();
        }

        /// <summary>
        /// Fire the event which indicates, that the messenger has checked
        /// for new messages.
        /// </summary>
        protected void fireHistoryLoadedEvent()
        {
            this.HistoryLoadedEvent?.Invoke();
        }

        public void Dispose()
        {
            this.messengerTasksTimer.Stop();
        }


        public delegate void NewMessageIncomingHandler(object sender, MessageIncoming message);
        public event NewMessageIncomingHandler NewMessageIncomingEvent;

        public delegate void NewMessageOutgoingHandler(object sender, MessageOutgoing message);
        public event NewMessageOutgoingHandler NewMessageOutgoingEvent;

        public delegate void CheckedForNewMessagesHandler();
        public event CheckedForNewMessagesHandler CheckedForNewMessagesEvent;

        public delegate void CheckForNewMessagesHandler();
        public event CheckForNewMessagesHandler CheckForNewMessagesEvent;

        public delegate void HistoryLoadedHandler();
        public event HistoryLoadedHandler HistoryLoadedEvent;
    }
}
