﻿using System;
using System.ComponentModel;

namespace PCM.MPC.Messenger.Data
{
    /// <summary>
    /// Specialisation of the Message class. Indicates an outgoing message.
    /// </summary>
    public class MessageOutgoing : Message, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Store for Status property.
        /// </summary>
        protected string status;

        /// <summary>
        /// Status property.
        /// </summary>
        /// <remarks>The status from the webservice. "ok" means, that 
        /// the message was sucessfully retrieved. This property
        /// sets also the IsError property.</remarks>
        public string Status
        {
            get
            {
                return this.status;
            }
            set
            {
                this.status = value;
                this.IsError = value != "ok";
                this.NotifyPropertyChanged("Status");
            }
        }

        /// <summary>
        /// Store for errorText.
        /// </summary>
        protected string errorText;

        /// <summary>
        /// ErrorText property.
        /// </summary>
        /// <remarks>The text of the occured error. Sets also the IsError property.</remarks>
        public string ErrorText
        {
            get
            {
                return this.errorText;
            }
            set
            {
                this.errorText = value;
                this.IsError = value != null && value.Length > 0;
                this.NotifyPropertyChanged("ErrorText");
            }
        }

        /// <summary>
        /// Store for IsError property.
        /// </summary>
        protected bool isError;

        /// <summary>
        /// IsError property.
        /// </summary>
        /// <remarks>Indicates that is an error occured. This can
        /// also be an internal app error.</remarks>
        public bool IsError
        {
            get
            {
                return this.isError;
            }
            set
            {
                this.isError = value;
                this.NotifyPropertyChanged("IsError");
            }
        }

        /// <summary>
        /// Store for IsDelivered property.
        /// </summary>
        protected bool isDelivered;

        /// <summary>
        /// IsDelivered property.
        /// </summary>
        /// <remarks>Indicates that this message is delivered
        /// to the webservice.</remarks>
        public bool IsDelivered
        {
            get
            {
                return this.isDelivered;
            }
            set
            {
                if (value)
                {
                    this.IsSubmitting = false;
                }

                this.isDelivered = value;
                this.NotifyPropertyChanged("IsDelivered");
            }
        }

        /// <summary>
        /// Store for IsSubmitting property.
        /// </summary>
        protected bool isSubmitting;

        /// <summary>
        /// IsSubmittin property.
        /// </summary>
        /// <remarks>Indicates that the message is on
        /// submitting state. This means, that the app is trying
        /// to send this message to the webservice.</remarks>
        public bool IsSubmitting
        {
            get
            {
                return this.isSubmitting;
            }
            set
            {
                this.isSubmitting = value;
                this.NotifyPropertyChanged("IsSubmitting");
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="toContact">Contact.</param>
        /// <param name="messageBody">Message.</param>
        public MessageOutgoing(Contact toContact, string messageBody) : this(toContact, messageBody, false)
        {
        }


        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="toContact">Contact.</param>
        /// <param name="messageBody">Message.</param>
        /// <param name="isDelivered">IsDelivered</param>
        public MessageOutgoing(Contact toContact, string messageBody, bool isDelivered) : this(toContact, messageBody, null, null, isDelivered)
        {
        }


        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="toContact">Contact.</param>
        /// <param name="messageBody">Message.</param>
        /// <param name="status">Status.</param>
        /// <param name="errorText">Error text.</param>
        /// <param name="isDelivered">IsDelivered</param>
        public MessageOutgoing(Contact toContact, string messageBody, string status, string errorText, bool isDelivered) : base(toContact, messageBody)
        {
            this.Status = status;
            this.ErrorText = errorText;
            this.IsDelivered = isDelivered;
        }

        /// <summary>
        /// Message was not submitted because of "cause".
        /// </summary>
        /// <param name="cause"></param>
        public void NotSubmitted(Exception cause)
        {
            this.IsSubmitting = false;
            this.IsDelivered = false;
            this.ErrorText = cause.Message;
        }

        /// <summary>
        /// Starts the submitting stage.
        /// </summary>
        public void StartSubmitting()
        {
            this.IsDelivered = false;
            this.IsSubmitting = true;
        }

        /// <summary>
        /// End submitting stage with delivered state.
        /// </summary>
        /// <param name="isDelivered">Delivered state.</param>
        public void EndSubmitting(bool isDelivered)
        {
            this.IsDelivered = isDelivered;
            this.IsSubmitting = false;
        }

        protected virtual void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
