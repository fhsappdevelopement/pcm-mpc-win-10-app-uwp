﻿using PCM.MPC.Pages.TalkService.Util;
using Windows.UI.Xaml.Media.Imaging;

namespace PCM.MPC.Messenger.Data
{
    /// <summary>
    /// Specialisation of the MessageIncoming. Contains ImageBody (Base64 String) and ImageBmp (BitmapImage)
    /// </summary>
    public class ImageMessageIncoming : MessageIncoming
    {
        /// <summary>
        /// Base64 String for Image
        /// </summary>
        public ImageDescription ImageDescription { get; private set; }

        /// <summary>
        /// store for BitmapImage property
        /// </summary>
        protected BitmapImage imageBmp;

        /// <summary>
        /// ImageBmp property
        /// </summary>
        public BitmapImage ImageBmp
        {
            get
            {
                if (imageBmp == null)
                {
                    this.imageBmp = ImageUtils.Base64ToBitmap(this.ImageDescription.ImageBody);
                }
                return this.imageBmp;

            }
            set
            {
                this.imageBmp = value;
            }
        }

        /// <summary>
        /// Constructor which calls partent constructor and sets ImageBody 
        /// </summary>
        /// <param name="fromContact">Contact of the receiver of the message</param>
        /// <param name="messageBody">string of the message</param>
        /// <param name="description">Base64 encoded imagebody with imagetyp, encapsulated in ImageDescription</param>
        public ImageMessageIncoming(Contact fromContact, string messageBody, ImageDescription description) : base(fromContact, messageBody)
        {
            this.ImageDescription = description;
        }
    }
}
