﻿using PCM.MPC.WCFRest.Data;
using System;
using System.ComponentModel;
using Windows.UI.Xaml.Media.Imaging;

namespace PCM.MPC.Messenger.Data
{
    /// <summary>
    /// Representates a contact with an image. It can be
    /// a normal contact as well as a division contact.
    /// </summary>
    public class Contact : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Store for ContactImage property.
        /// </summary>
        protected BitmapImage contactImage;

        /// <summary>
        /// ContactImage property.
        /// </summary>
        public BitmapImage ContactImage
        {
            get
            {
                return this.contactImage;
            }
            set
            {
                this.contactImage = value;
                NotifyPropertyChanged("ContactImage");
            }
        }

        /// <summary>
        /// Store for Lastname property.
        /// </summary>
        protected string lastname;

        /// <summary>
        /// Lastname Property.
        /// </summary>
        public string Lastname
        {
            get
            {
                return this.lastname;
            }
            set
            {
                this.lastname = value;
                NotifyPropertyChanged("Lastname");
            }
        }

        /// <summary>
        /// Store for Firstname property.
        /// </summary>
        protected string firstname;

        /// <summary>
        /// Firstname property.
        /// </summary>
        public string Firstname
        {
            get
            {
                return this.firstname;
            }
            set
            {
                this.firstname = value;
                NotifyPropertyChanged("Firstname");
            }
        }

        /// <summary>
        /// Store for Division property.
        /// </summary>
        protected string division;

        /// <summary>
        /// Division property.
        /// </summary>
        public string Division
        {
            get
            {
                return this.division;
            }
            set
            {
                this.division = value;
                NotifyPropertyChanged("Division");
            }
        }

        /// <summary>
        /// Store for HasNewMessage property.
        /// </summary>
        protected bool hasNewMessage;

        /// <summary>
        /// HasNewMessage property.
        /// </summary>
        /// <remarks>Indicates, that this contact has send one or more new messages.</remarks>
        public bool HasNewMessage
        {
            get
            {
                return this.hasNewMessage;
            }
            set
            {
                this.hasNewMessage = value;
                NotifyPropertyChanged("HasNewMessage");
            }
        }

        /// <summary>
        /// Store for PSID property.
        /// </summary>
        protected int psid;

        /// <summary>
        /// PSID property.
        /// </summary>
        public int PSID
        {
            get// PSID should never be changed
            {
                return this.psid;
            }
        }

        /// <summary>
        /// State Property.
        /// </summary>
        public ContactState State { get; private set; } // private set to prevent reference pollution

        /// <summary>
        /// FullName property. Concatinates Firstname and Lastname with a whitespace.
        /// </summary>
        public string FullName
        {
            get
            {
                return this.Firstname + " " + this.Lastname;
            }
        }

        /// <summary>
        /// IsRemoveable property. Indicates if this contact is removeable from ContactList.
        /// </summary>
        /// <remarks>This property should always returns false. But, sometimes it is nice
        /// to mark a contact as not removeable. Then you can simply set this property
        /// to true.</remarks>
        public bool IsRemoveable { get; set; }

        /// <summary>
        /// Displays the fullname and the division name 
        /// </summary>
        public string DisplayText
        {
            get
            {
                if (this.IsDivision())
                {
                    return "Abteilung: " + this.Division;
                }

                return this.FullName + "\n" + this.Division;
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="contact"></param>
        /// <param name="contactImage"></param>
        public Contact(TalkContact contact, BitmapImage contactImage)
            : this(contact.Nachname, contact.Vorname, contact.Abteilung, contact.PSID, contact.StatusID, contactImage)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="lastname">Lastname.</param>
        /// <param name="firstname">Firstname.</param>
        /// <param name="divisionName">Name of the division.</param>
        /// <param name="PSID">PSID.</param>
        /// <param name="stateID">Contact state.</param>
        public Contact(string lastname, string firstname, string divisionName, int PSID, int stateID)
            : this(lastname, firstname, divisionName, PSID, stateID, null)
        {

        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="lastname">Lastname.</param>
        /// <param name="firstname">Firstname.</param>
        /// <param name="divisionName">Name of the division.</param>
        /// <param name="PSID">PSID.</param>
        /// <param name="stateID">Contact state.</param>
        /// <param name="contactImage">Contact image.</param>
        public Contact(string lastname, string firstname, string divisionName, int PSID, int stateID, BitmapImage contactImage)
            : this(lastname, firstname, divisionName, PSID, new ContactState(stateID), contactImage, true)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="lastname">Lastname.</param>
        /// <param name="firstname">Firstname.</param>
        /// <param name="divisionName">Name of the division.</param>
        /// <param name="PSID">PSID.</param>
        /// <param name="stateID">Contact state.</param>
        /// <param name="contactImage">Contact image.</param>
        /// <param name="isRemoveable">Determines if this contact is removeable.</param>
        public Contact(string lastname, string firstname, string divisionName, int PSID, int stateID, BitmapImage contactImage, bool isRemoveable)
            : this(lastname, firstname, divisionName, PSID, new ContactState(stateID), contactImage, isRemoveable)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="lastname">Lastname.</param>
        /// <param name="firstname">Firstname.</param>
        /// <param name="divisionName">Name of the division.</param>
        /// <param name="PSID">PSID.</param>
        /// <param name="state">Contact state.</param>
        /// <param name="contactImage">Contact image.</param>
        /// <param name="isRemoveable">Determines if this contact is removeable.</param>
        public Contact(string lastname, string firstname, string divisionName, int PSID, ContactState.States state, BitmapImage contactImage, bool isRemoveable)
            : this(lastname, firstname, divisionName, PSID, new ContactState(state), contactImage, isRemoveable)
        {
        }


        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="lastname">Lastname.</param>
        /// <param name="firstname">Firstname.</param>
        /// <param name="divisionName">Name of the division.</param>
        /// <param name="PSID">PSID.</param>
        /// <param name="state">Contact state.</param>
        /// <param name="contactImage">Contact image.</param>
        /// <param name="isRemoveable">Determines if this contact is removeable.</param>
        public Contact(string lastname, string firstname, string divisionName, int PSID, ContactState state, BitmapImage contactImage, bool isRemoveable)
        {
            this.Lastname = lastname;
            this.Firstname = firstname;
            this.Division = divisionName;
            this.psid = PSID;
            this.State = state;
            this.ContactImage = contactImage;
            this.hasNewMessage = false;
            this.IsRemoveable = isRemoveable;
        }

        /// <summary>
        /// Updates this contact based on an another contact.
        /// </summary>
        /// <remarks>This update method is used to keep the reference of 
        /// the original object and to keep the positon in the contactlist.</remarks>
        /// <param name="contact">Contact which will update this contact.</param>
        /// <returns>true if this contact is updated.</returns>
        public bool Update(Contact contact)
        {
            if (contact.psid != this.psid)
            {
                throw new Exception("Tried to change PSID through another Contact object on update.");
            }

            // only for simplifying code
            IsUpdated isUpdated = new IsUpdated();
            if (isUpdated.Assert(contact.lastname != this.lastname))
            {
                // important to use Property here, because of the NotifyPropertyChanged
                this.Lastname = contact.Lastname;
            }

            if (isUpdated.Assert(contact.firstname != this.firstname))
            {
                this.Firstname = contact.Firstname;
            }

            if (isUpdated.Assert(contact.division != this.division))
            {
                this.Division = contact.Division;
            }

            // Enum comparison
            if (isUpdated.Assert(this.State.State != contact.State.State))
            {
                this.State.ChangeStateTo(contact.State.State);
            }

            return isUpdated;
        }

        /// <summary>
        /// Determines if this contact is a division.
        /// </summary>
        /// <returns></returns>
        public bool IsDivision()
        {
            return this.PSID < 0;
        }

        protected virtual void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public override bool Equals(Object obj)
        {
            Contact contact = obj as Contact;

            if (contact == null)
            {
                return false;
            }

            // only check for psid, because it's the same contact if the psid matches
            // the update method will show up which values should be updated
            return this.psid == contact.psid;
        }

        public override int GetHashCode()
        {
            // psid should work as hashcode
            return this.psid;
        }

        /// <summary>
        /// Inner class which will be used for the update methode
        /// to reduce code redundancy.
        /// </summary>
        private class IsUpdated
        {
            private bool isUpdated;

            public IsUpdated()
            {
                isUpdated = false;
            }

            public bool Assert(bool expr)
            {
                this.isUpdated |= expr;

                return expr;
            }

            public static implicit operator bool(IsUpdated t)
            {
                return t.isUpdated;
            }
        }
    }
}
