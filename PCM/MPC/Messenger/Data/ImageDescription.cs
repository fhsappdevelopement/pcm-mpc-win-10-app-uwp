﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCM.MPC.Messenger.Data
{
    public class ImageDescription
    {
        public string ImageBody { get; set; }
        public string ImageTyp { get; set; }

        public ImageDescription(string body, string typ)
        {
            this.ImageBody = body;
            this.ImageTyp = typ;
        }
    }
}
