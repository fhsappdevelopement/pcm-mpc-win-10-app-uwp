﻿using System;
using System.ComponentModel;

namespace PCM.MPC.Messenger.Data
{
    /// <summary>
    /// Representates the online state of a contact.
    /// </summary>
    /// <seealso cref="Contact"/>
    public class ContactState : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Enumeration based on the PCM guidelines.
        /// </summary>
        public enum States : int { ALSO_OFFLINE, AVAILABLE, OCCUPIED, OFFLINE, HAS_APPOINTMENT, DONT_DISTURB };

        /// <summary>
        /// Store for state.
        /// </summary>
        protected States state;

        /// <summary>
        /// State property.
        /// </summary>
        public States State
        {
            get
            {
                return this.state;
            }
        }

        /// <summary>
        /// Store for StatusID property.
        /// </summary>
        protected int statusID;

        /// <summary>
        /// StatusID property. Deprecated.
        /// </summary>
        /// <remarks>The set method will use the ChangeStateTo-method.</remarks>
        public int StatusID
        {
            get
            {
                return this.statusID;
            }
            set
            {
                this.ChangeStateTo(value);
            }
        }

        /// <summary>
        /// Constructor. </summar<>
        /// <remarks>
        /// 0 = abgemeldet
        /// 1 = frei
        /// 2 = besetzt
        /// 3 = abgemeldet
        /// 4 = Termin
        /// 5 = nicht stören
        /// </remarks>
        /// <param name="statusID">State ID.</param>
        public ContactState(int statusID)
        {
            this.ChangeStateTo(statusID);
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="state">State.</param>
        public ContactState(States state)
        {
            this.ChangeStateTo(state);
        }

        /// <summary>
        /// Changes the state to the desired int-state.
        /// </summary>
        /// <remarks>Throws an exception if state is greater than 
        /// five or lower than zero.</remarks>
        /// <param name="state">State ID</param>
        public void ChangeStateTo(int state)
        {
            if (state < 0 || state > 5)
            {
                throw new Exception("Unknown state.");
            }

            this.ChangeStateTo((States)Enum.ToObject(typeof(States), state));
        }

        /// <summary>
        /// Changes the state.
        /// </summary>
        /// <param name="state">State.</param>
        public void ChangeStateTo(States state)
        {
            this.statusID = (int)state;
            this.state = state;

            this.NotifyPropertyChanged("StatusID");
            this.NotifyPropertyChanged("State");
        }

        /// <summary>
        /// Checks the State.
        /// </summary>
        /// <returns>true if available.</returns>
        public bool IsAvailable()
        {
            return this.state == States.AVAILABLE;
        }

        /// <summary>
        /// Checks the State.
        /// </summary>
        /// <returns>true if occupied.</returns>
        public bool IsOccupied()
        {
            return this.state == States.OCCUPIED;
        }

        /// <summary>
        /// Checks the State.
        /// </summary>
        /// <returns>true if offline</returns>
        public bool IsOffline()
        {
            return this.state == States.OFFLINE || this.StatusID == (int)States.ALSO_OFFLINE;
        }

        /// <summary>
        /// Checks the State.
        /// </summary>
        /// <returns>true if has an appointment.</returns>
        public bool IsHasAppointment()
        {
            return this.state == States.HAS_APPOINTMENT;
        }

        /// <summary>
        /// Checks the State.
        /// </summary>
        /// <returns>true if on dont disturb.</returns>
        public bool IsDontDisturb()
        {
            return this.state == States.DONT_DISTURB;
        }

        protected virtual void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
