﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace PCM.MPC.Messenger.Data
{
    /// <summary>
    /// History for the different specialisation of Message, which 
    /// will all be stored as Message instances. 
    /// </summary>
    /// <remarks>Messages are organized by the psid of the contacts inside 
    /// a ObservableCollection.</remarks>
    public class MessageHistory
    {
        /// <summary>
        /// Store for messages.
        /// </summary>
        protected Dictionary<int, ObservableCollection<Message>> historyStore;

        public Dictionary<int, ObservableCollection<Message>> HistoryStore
        {
            get
            {
                return historyStore;
            }
            private set
            {
                this.historyStore = value;
            }
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public MessageHistory()
        {
            this.historyStore = new Dictionary<int, ObservableCollection<Message>>();
        }

        /// <summary>
        /// Adds a new message to the history.
        /// </summary>
        /// <param name="message">Message.</param>
        public void AddMessage(Message message)
        {
            if (message.Contact == null)
            {
                throw new System.Exception("Message without Contact!");
            }

            var psid = message.Contact.PSID;

            this.initListIfNeeded(psid);
            this.historyStore[psid].Add(message);
        }

        /// <summary>
        /// Adds a list of messages to the history.
        /// </summary>
        /// <param name="messages">List of message.</param>
        public void AddMessages(List<Message> messages)
        {
            foreach (var message in messages)
            {
                this.AddMessage(message);
            }
        }

        /// <summary>
        /// Overloaded array access operator.
        /// </summary>
        /// <param name="psid">PSID of a contact.</param>
        /// <returns>Collection for the specific contact.</returns>
        public ObservableCollection<Message> this[int psid]
        {
            get
            {
                this.initListIfNeeded(psid);
                return this.historyStore[psid];
            }
        }

        /// <summary>
        /// Inits the list on demand.
        /// </summary>
        /// <param name="psid">PSID</param>
        protected void initListIfNeeded(int psid)
        {
            if (!this.historyStore.ContainsKey(psid))
            {
                this.historyStore.Add(psid, new ObservableCollection<Message>());
            }
        }

        /// <summary>
        /// Erases the history for a specific contact.
        /// </summary>
        /// <param name="psid">PSID</param>
        public void ClearHistoryForPSID(int psid)
        {
            this.historyStore.Remove(psid);
        }

        /// <summary>
        /// Erases the whole history.
        /// </summary>
        public void clearHistoryForAllContacts()
        {
            this.historyStore.Clear();
        }
    }
}
