﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;

namespace PCM.MPC.Messenger.Data
{
    /// <summary>
    /// Simple Class to manage Contact instances. List can be updated by calling
    /// update(contact : ArrayOfTalkContact).
    /// </summary>
    /// <remarks>This list also fires a contact replaced event if a contact
    /// is updated through the Contact.Update method.</remarks>
    public class ContactList : ObservableCollection<Contact>
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ContactList()
        {
        }

        public new void Add(Contact contact)
        {
            if (this.Contains(contact))
            {
                throw new Exception("Contact with PSID " + contact.PSID + " already contained in this contactlist.");
            }

            base.Add(contact);
        }

        /// <summary>
        /// Searches for a contact which will match the psid.
        /// </summary>
        /// <param name="psid">PSID.</param>
        /// <returns>The desired contact, null if not founded.</returns>
        public Contact GetContactByPsID(int psid)
        {
            var list = this.Where(c => c.PSID == psid).ToList();

            if (list.Any())
            {
                return list.First();
            }

            return null;
        }

        /// <summary>
        /// This method checks if Contacts must be removed or must be added.
        /// Existing contacts would be updated by Contact.update method
        /// </summary>
        /// <param name="contacts"></param>
        public void Update(ContactList contacts)
        {
            // remove contacts which are not included in contacts 
            var diff = this.Except(contacts).ToList();
            foreach (Contact contact in diff)
            {
                if (contact.IsRemoveable)
                {
                    this.Remove(contact);
                }
            }

            // update contacts
            foreach (Contact contact in contacts)
            {
                if (this.Contains(contact))
                {
                    //var toUpdate = this.Where(c => c.PSID == contact.PSID).ToList().First(); 
                    var index = this.IndexOf(contact);
                    var toUpdate = this[index];


                    var shouldFireEvent = toUpdate.Update(contact) || toUpdate.HasNewMessage;
                    // fire event for the changed element // doesn't work, it will throw an exception (MSDN fault)
                    // http://www.codeproject.com/Articles/1004644/ObservableCollection-Simply-Explained
                    if (shouldFireEvent)
                    { // maybe a good fix
                        base.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, toUpdate, toUpdate, index));
                    }
                }
                else
                {
                    this.Add(contact);
                }
            }

        }
    }
}
