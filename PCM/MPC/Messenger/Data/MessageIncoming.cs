﻿namespace PCM.MPC.Messenger.Data
{
    /// <summary>
    /// Specialisation of the Message class. Indicates an incoming message.
    /// </summary>
    public class MessageIncoming : Message
    {
        /// <summary>
        /// Calls the parent constructor.
        /// </summary>
        /// <param name="fromContact">Sender of this message.</param>
        /// <param name="messageBody">Message.</param>
        public MessageIncoming(Contact fromContact, string messageBody) : base(fromContact, messageBody) { }
    }
}
