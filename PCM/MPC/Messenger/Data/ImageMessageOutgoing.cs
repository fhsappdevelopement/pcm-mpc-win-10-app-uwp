﻿using PCM.MPC.Pages.TalkService.Util;
using Windows.UI.Xaml.Media.Imaging;

namespace PCM.MPC.Messenger.Data
{
    /// <summary>
    /// Specialisation of the MessageOutgoing. Contains ImageBody (Base64 String) and ImageBmp (BitmapImage)
    /// </summary>
    public class ImageMessageOutgoing : MessageOutgoing
    {
        /// <summary>
        /// Base64 String for Image
        /// </summary>
        public ImageDescription ImageDescription { get; set; }

        /// <summary>
        /// store for BitmapImage property
        /// </summary>
        protected BitmapImage imageBmp;

        /// <summary>
        /// ImageBmp property
        /// </summary>
        public BitmapImage ImageBmp
        {
            get
            {
                if (imageBmp == null)
                {
                    this.imageBmp = ImageUtils.Base64ToBitmap(this.ImageDescription.ImageBody);
                }
                return this.imageBmp;

            }
            set
            {
                this.imageBmp = value;
            }
        }

        /// <summary>
        /// Constructor which calls partent constructor and sets ImageBody 
        /// </summary>
        /// <param name="toContact">Contact of the sender of the message</param>
        /// <param name="messageBody">string of the message</param>
        /// <param name="imagebody">base64 string of the image</param>
        public ImageMessageOutgoing(Contact toContact, string messageBody, ImageDescription description) : base(toContact, messageBody)
        {
            this.ImageDescription = description;
        }

        /// <summary>
        /// Constructor which calls partent constructor and sets other values for the database
        /// </summary>
        /// <param name="toContact">Contact of the sender of the message</param>
        /// <param name="messageBody">string of the message</param>
        /// <param name="imageBody">base64 string of the image</param>
        /// <param name="status">status of the message <see cref="MessageOutgoing"/></param>
        /// <param name="errorText">errortext <see cref="MessageOutgoing"/></param>
        /// <param name="isDelivered">bool value if message is delivered <see cref="MessageOutgoing"/></param>
        public ImageMessageOutgoing(Contact toContact, string messageBody, ImageDescription description, string status, string errorText, bool isDelivered) :
            base(toContact, messageBody, status, errorText, isDelivered)
        {
            this.ImageDescription = description;
        }
    }
}
