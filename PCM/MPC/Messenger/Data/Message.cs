﻿namespace PCM.MPC.Messenger.Data
{
    /// <summary>
    /// Abstract class which generalizes a message.
    /// </summary>
    public abstract class Message
    {
        /// <summary>
        /// Contact property.
        /// </summary>
        /// <remarks>The sender or receiver of this message.</remarks>
        /// <seealso cref="MessageIncoming"/>
        /// <seealso cref="MessageOutgoing"/>
        public Contact Contact { get; set; }

        /// <summary>
        /// MessageBody property.
        /// </summary>
        /// <remarks>The whole message.</remarks>
        public string MessageBody { get; set; }


        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="contact">Contact.</param>
        /// <param name="messageBody">Message.</param>
        public Message(Contact contact, string messageBody)
        {
            this.Contact = contact;
            this.MessageBody = messageBody;
        }
    }
}
