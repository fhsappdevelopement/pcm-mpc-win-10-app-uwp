﻿using PCM.MPC.Messenger;
using PCM.MPC.WCFRest;
using PCM.MPC.WCFRest.Data;
using System;
using System.Threading.Tasks;

namespace PCM.MPC
{
    /// <summary>
    /// Main class for the business logic. Holds the REST connectivity,
    /// authenticate credentials, manage the authentication and logout 
    /// process, get access to the app components (talkservice).
    /// </summary>
    public class MobileProcarementCenter
    {
        /// <summary>
        /// restService instance.
        /// </summary>
        protected IMPCRestService restService;

        /// <summary>
        /// Used authentication Credentials.
        /// </summary>
        protected MPCCredentials credentials;

        /// <summary>
        /// Store for messenger property.
        /// </summary>
        protected MPCMessenger messenger;

        /// <summary>
        /// Messenger property.
        /// </summary>
        /// <remarks>If you access this property, your authentication state will be checked.
        /// If you are not authenticated, an exception will be thrown!
        /// Messenger is created on demand if you'r successfully authenticated.</remarks>
        public MPCMessenger Messenger
        {
            get
            {
                checkAuthentication();

                if (this.messenger == null)
                {
                    this.messenger = new MPCMessenger(this.restService, this.userInfo);
                }

                return this.messenger;
            }
        }

        /// <summary>
        /// Store for UserInfo property.
        /// </summary>
        protected UserInfo userInfo;

        /// <summary>
        /// UserInfo property.
        /// </summary>
        /// <seealso cref="UserInfo"/>
        public UserInfo UserInfo
        {
            get
            {
                return this.userInfo;
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="restService">REST Service.</param>
        /// <seealso cref="App"/>
        public MobileProcarementCenter(IMPCRestService restService)
        {
            this.restService = restService;
        }

        /// <summary>
        /// Authenticates the user with the entered credentials against the webservices. Throwns an exception if the login is failed.
        /// </summary>
        /// <param name="credentials">Credentials.</param>
        /// <returns>Only task for async purposes.</returns>
        public async Task Authenticate(MPCCredentials credentials)
        {
            var infoResponse = await this.restService.InfoGetItem(credentials);

        if (infoResponse.SessionGUID == null || infoResponse.SessionGUID.Length == 0)
        {
            throw new Exception("SessionGUID not available.");
        }

            this.userInfo = new UserInfo(infoResponse.SessionGUID, infoResponse.MID, infoResponse.MpcPsID, credentials.Username);
            this.test(userInfo);
        }

        public async void test(UserInfo info)
        {
            // BS
            // 5.1.1 EklLieferant GetItems Request
            ArrayOfDelivery eklDelivery = await this.restService.EklDeliveryGetItems(info.MID, info.SessionGUID);
            // 5.2.1 Ekl GetItems
            ArrayOfEkl eklList = await this.restService.EklGetItems(info.MID, info.SessionGUID);
            // 5.3.1 EklPosition GetItems
            ArrayOfEklPosition eklPositions = await this.restService.EklPositionGetItems(info.MID, 72042, info.SessionGUID);
            // 5.4.1.1 LIDArticle GetItembyArtNr
            ArrayOfLIDArticle lidByArtNr = await this.restService.LIDArticleGetItemByArticleNumber(info.MID, 4711, info.SessionGUID);
            // 5.4.1.2 LIDArticle GetItembyLID
            ArrayOfLIDArticle lidByLID = await this.restService.LIDArticleGetItemByLId(info.MID, 853, info.SessionGUID);
            // 5.4.1.3 LIDArticle GetItembyBezPCM
            ArrayOfLIDArticle lidByPCM = await this.restService.LIDArticleGetItemByDescription(info.MID, "Steak", info.SessionGUID);

            // PROD
            // 7.1.1 RzHierarchie GetITems Request
            ArrayOfRzGrpHaupt grp = await this.restService.RzHierarchieGetItems(info.MID, info.SessionGUID);
            // 7.2.1 Rz GetItems Request
            ArrayOfRz rzs = await this.restService.RzGetItems(info.MID, info.SessionGUID);
            // 7.3.1 RzPos
            ArrayOfRzPos arrRzPosMatchedByID = await this.restService.RzPosGetItemById(info.MID, 140473, info.SessionGUID);
            // 7.5.1 Mnu GetItems
            ArrayOfMnu arrMnu = await this.restService.MnuGetItems(info.MID, info.SessionGUID);
            // 7.5.2 Mnu GetItemByID
            ArrayOfMnu mnuByID = await this.restService.MnuGetItemById(info.MID, 25438, info.SessionGUID);
            // 7.6.1 MnuPos GetItembyID
            ArrayOfMnuPos arrMnuPos = await this.restService.MnuPosGetItemById(info.MID, 25438, info.SessionGUID);
            // 7.8.1 Tp GetItems 
            ArrayOfTp arrofTp = await this.restService.TpGetItems(info.MID, info.SessionGUID);
            // 7.8.2 Tp GetItembyID
            ArrayOfTp arrtpByID = await this.restService.TpGetItemById(info.MID, 57, info.SessionGUID);
            // 7.9.1 TpPos GetItemById
            ArrayOfTpPos arrOfTpByID = await this.restService.TpPosGetItemById(info.MID, 57, info.SessionGUID);
            // 7.11.1 Wp GetItems
            ArrayOfWp arrOfWp = await this.restService.WpGetItems(info.MID, info.SessionGUID);
            // 7.11.2 Wp GetItembyID
            ArrayOfWp arrOfWpByID = await this.restService.WpGetItemById(info.MID, 8, info.SessionGUID);
            // 7.12.1 WpPos GetItemById
            ArrayOfWpPos arrOfWpPos = await this.restService.WpPosGetItemById(info.MID, 8, info.SessionGUID);
        }

        /// <summary>
        /// Checks if the user is authenticated.
        /// </summary>
        /// <returns>true if successfully authenticated, false otherwise.</returns>
        public bool IsAuthenticated()
        {
            return this.userInfo != null && this.userInfo.SessionGUID != null && this.userInfo.SessionGUID.Length > 0;
        }

        /// <summary>
        /// Checks the authentication state. 
        /// </summary>
        protected void checkAuthentication()
        {
            if (!this.IsAuthenticated())
            {
                throw new Exception("Not authenticated.");
            }
        }

        /// <summary>
        /// Saves the history into a database and destroys the userinfo and messenger object.
        /// </summary>
        /// <returns></returns>
        public async Task Logout()
        {
            // destory userinfo
            this.userInfo = null;

            // dispose messenger first
            if (this.messenger != null)
            {
                this.messenger.Dispose();
                this.messenger = null;
            }
        }
    }
}
